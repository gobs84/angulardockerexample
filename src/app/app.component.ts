import { Component } from '@angular/core';
import { Http } from '@angular/http'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'AngularDockerWS';
  constructor(private _httpService: Http) { }
  accessPointUrl: string = 'http://localhost:8888/api/';
  apiValues: string[] = [];
  profiles = [];
  ngOnInit() {
    this._httpService.get(this.accessPointUrl+'Values').subscribe(values => {
      this.apiValues = values.json() as string[];
    });

    this._httpService.get(this.accessPointUrl+'Profiles').subscribe(values => {
      this.profiles = values.json() as [];
    });
  }
}
